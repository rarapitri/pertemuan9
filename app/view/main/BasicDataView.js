Ext.define('pertemuan9.view.main.BasicDataView', {
    extend: 'Ext.Container',
    xtype : 'detail', 
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'pertemuan9.store.Personnel'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    viewModel: {
        stores: {
          personnel:{
                type:'personnel'
            }
        }
    },

    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        id:'mydataview',
        cls: 'dataview-basic',
        itemTpl: 'Name : {name}<br>Email :{email}<br>Phone:{phone} <br><button type=button onclick="onUpdatePersonnel({user_id})">Edit</button><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button><hr> ',
       bind: {
            store: '{personnel}'
        },
       //  plugins: {
       //      type: 'dataviewtip',
       //      align: 'l-r?',
       //      plugins: 'responsive',
            
       //      // On small form factor, display below.
       //      responsiveConfig: {
       //          "width < 600": {
       //              align: 'tl-bl?'
       //          }
       //      },
       //      width: 600,
       //      minWidth: 300,
       //      //delegate: '.img',
       //      allowOver: true,
       //      anchor: true,
       //      bind: '{record}',
       //      tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
       //              '<tr><td>Name: </td><td>{name}</td></tr>' +
       //              '<tr><td>Email:</td><td>{email}</td></tr>' + 
       //              '<tr><td>Phone:</td><td>{phone}</td></tr>' 
                    
       // }
    }]
});